package dbmodels

import "time"

type TbAbsenNew struct {
	ID          int       `gorm:"column:id"`
	Nik         int       `gorm:"column:nik"`
	Departement string    `gorm:"column:departement"`
	AcNo        int       `gorm:"column:ac_no"`
	Nama        string    `gorm:"column:nama"`
	Tanggal     time.Time `gorm:"column:tanggal"`
	Shift       string    `gorm:"column:shift"`
	ClockIn     string    `gorm:"column:clock_in"`
	StartTime   string    `gorm:"column:start_time"`
	EndTime     string    `gorm:"column:end_time"`
	ClockOut    string    `gorm:"column:clock_out"`
	Exception   string    `gorm:"column:exception"`
	FlagKerja   int       `gorm:"column:flag_kerja"`
	Status      string    `gorm:"column:status"`
	InputDate   time.Time `gorm:"column:inputdate"`
	UpdateDate  time.Time `gorm:"column:updatedate"`
}

func (t *TbAbsenNew) TableName() string {
	return "tb_absen_new"
}
