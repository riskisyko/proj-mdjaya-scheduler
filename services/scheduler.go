package services

import (
	"fmt"
	"proj-medanjaya-scheduler/models"
	"proj-medanjaya-scheduler/models/dbmodels"
	"time"

	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
	gormbulk "github.com/t-tiger/gorm-bulk-insert"
)

type Scheduler struct {
	MySQLConn *gorm.DB
	MSSQLConn *gorm.DB
}

func (s *Scheduler) Migrate() {
	lastData := []*dbmodels.TbAbsenNew{}
	queryGetLastData := "select * from tb_absen_new order by id desc limit 1"
	if err := s.MySQLConn.Raw(queryGetLastData).Scan(&lastData).Error; err != nil {
		logrus.Error(err.Error())
	}
	var startDate time.Time
	startDate = time.Now()
	if len(lastData) != 0 {
		startDate = lastData[0].InputDate
	}
	response := []*models.AbsenSPResponse{}

	queryExecuteSP := fmt.Sprintf(`exec dbo.sp_absen @PAGESIZE = 0, @PAGENUMBER = 0, @ID = 0, @STARTDATE = '%s', @ENDDATE = ''`,
		startDate.Format("2006-01-02 15:04:05"))

	if err := s.MSSQLConn.Raw(queryExecuteSP).Scan(&response).Error; err != nil {
		logrus.Error(err.Error())
	}

	if len(response) == 0 {
		logrus.Info("There is no new data")
	}

	var insertRecords []interface{}
	for i := 0; i < len(response); i++ {
		if response[i].ClockIn == "" {
			response[i].ClockIn = "00:00:00"
		}
		if response[i].StartTime == "" {
			response[i].StartTime = "00:00:00"
		}
		if response[i].EndTime == "" {
			response[i].EndTime = "00:00:00"
		}
		if response[i].ClockOut == "" {
			response[i].ClockOut = "00:00:00"
		}
		insertRecords = append(insertRecords,
			dbmodels.TbAbsenNew{
				Departement: response[i].Departement,
				Nama:        response[i].Nama,
				AcNo:        response[i].ID,
				Nik:         response[i].Nik,
				Tanggal:     response[i].Tanggal,
				ClockIn:     response[i].ClockIn,
				StartTime:   response[i].StartTime,
				ClockOut:    response[i].ClockOut,
				EndTime:     response[i].EndTime,
				Exception:   response[i].Exception,
				InputDate:   time.Now(),
				UpdateDate:  time.Now(),
			},
		)
	}
	if err := gormbulk.BulkInsert(s.MySQLConn, insertRecords, 3000); err != nil {
		logrus.Error(err)
	}
}
