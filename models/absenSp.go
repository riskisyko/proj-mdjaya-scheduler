package models

import "time"

type AbsenSPResponse struct {
	Num         int       `json:"num"`
	ID          int       `json:"id"`
	Nama        string    `json:"nama"`
	Departement string    `json:"departement"`
	Nik         int       `json:"nik"`
	Tanggal     time.Time `json:"tanggal"`
	ClockIn     string    `json:"clockIn"`
	StartTime   string    `json:"startTime"`
	EndTime     string    `json:"endTime"`
	ClockOut    string    `json:"clockOut"`
	Exception   string    `json:"exception"`
}
