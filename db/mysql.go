package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

type EnvMYSQLDb struct {
	User  string `envconfig:"MYSQL_USER" default:"user"`
	Pass  string `envconfig:"MYSQL_PASS" default:"user"`
	Name  string `envconfig:"MYSQL_NAME" default:"proj-mdjaya"`
	Host  string `envconfig:"MYSQL_HOST" default:"192.168.64.2"`
	Port  int    `envconfig:"MYSQL_PORT" default:"3306"`
	Debug bool   `envconfig:"MYSQL_DEBUG" default:"true"`
}

var (
	DbConMysql *gorm.DB
	DbErrMysql error
	envMYSQLDb EnvMYSQLDb
)

func init() {
	err := envconfig.Process("MDJaya", &envMYSQLDb)
	if err != nil {
		logrus.Error("Failed to get MDJaya env:", err)
	}

	conn, err := DbOpenMysql()
	if err != nil {
		logrus.Error("Can't open", envMYSQLDb.Name, "DB")
	}
	DbConMysql = conn
	conn.LogMode(envMYSQLDb.Debug)
}

// DbOpen ..
func DbOpenMysql() (*gorm.DB, error) {
	dsn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", envMYSQLDb.User, envMYSQLDb.Pass, envMYSQLDb.Host, envMYSQLDb.Port, envMYSQLDb.Name)
	DbConMysql, DbErrMysql := gorm.Open("mysql", dsn)
	if DbErrMysql != nil {
		return nil, DbErrMysql
	}
	if err := DbConMysql.DB().Ping(); err != nil {
		return nil, err
	}
	return DbConMysql, nil
}
