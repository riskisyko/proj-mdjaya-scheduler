package main

import (
	"proj-medanjaya-scheduler/db"
	"proj-medanjaya-scheduler/services"

	"github.com/jasonlvhit/gocron"
	"github.com/jinzhu/gorm"
	"github.com/sirupsen/logrus"
)

func main() {
	if err := db.DbConMssql.DB().Ping(); err != nil {
		return
	}

	logrus.Info("Sql Server Connected")

	if err := db.DbConMysql.DB().Ping(); err != nil {
		return
	}

	logrus.Info("My Sql Connected")

	// Scheduler section
	gocron.Every(5).Minute().Do(MigrateData, db.DbConMssql, db.DbConMysql)

	<-gocron.Start()
}

func MigrateData(mssql, mysql *gorm.DB) {
	services := new(services.Scheduler)
	services.MSSQLConn = mssql
	services.MySQLConn = mysql
	services.Migrate()
}
