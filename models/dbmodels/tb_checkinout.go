package dbmodels

import "time"

type CheckInOut struct {
	UserID     int       `gorm:"column:USERID"`
	CheckTime  time.Time `gorm:"column:CHECKTIME"`
	CheckType  string    `gorm:"column:CHECKTYPE"`
	VerifyCode int       `gorm:"column:VERIFYCODE"`
	SensorID   string    `gorm:"column:SENSORID"`
	WorkCode   int       `gorm:"column:WORKCODE"`
	SN         string    `gorm:"column:sn"`
	UserExtFmt string    `gorm:"column:UserExtFmt"`
}

func (t *CheckInOut) TableName() string {
	return "CHECKINOUT"
}
