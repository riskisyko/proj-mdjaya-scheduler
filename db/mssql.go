package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mssql"
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

type EnvMSSQLDb struct {
	User  string `envconfig:"MSSQL_USER" default:"sa"`
	Pass  string `envconfig:"MSSQL_PASS" default:"qweASD123!!!"`
	Name  string `envconfig:"MSSQL_NAME" default:"mdjaya"`
	Host  string `envconfig:"MSSQL_HOST" default:"192.168.1.32"`
	Port  int    `envconfig:"MSSQL_PORT" default:"1433"`
	Debug bool   `envconfig:"MSSQL_DEBUG" default:"true"`
}

var (
	DbConMssql *gorm.DB
	DbErrMssql error
	envMSSQLDb EnvMSSQLDb
)

func init() {
	err := envconfig.Process("MDJaya", &envMSSQLDb)
	if err != nil {
		logrus.Error("Failed to get MDJaya env:", err)
	}

	if DbOpenMssql() != nil {
		logrus.Error("Can't open", envMSSQLDb.Name, "DB")
	}
	DbConMssql = GetDbConMssql()
	DbConMssql = DbConMssql.LogMode(envMSSQLDb.Debug)

}

// DbOpen ..
func DbOpenMssql() error {
	connectionString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s",
		envMSSQLDb.Host, envMSSQLDb.User, envMSSQLDb.Pass, envMSSQLDb.Port, envMSSQLDb.Name)
	DbConMssql, DbErrMssql = gorm.Open("mssql", connectionString)

	if DbErrMssql != nil {
		logrus.Error("Open", envMSSQLDb.Name, "DB error :", DbErrMssql)
		return DbErrMssql
	}

	if errping := DbConMssql.DB().Ping(); errping != nil {
		logrus.Error(errping)
		return errping
	}
	return nil
}

// GetDbCon ..
func GetDbConMssql() *gorm.DB {
	if errping := DbConMssql.DB().Ping(); errping != nil {
		logrus.Info("DB not connected test ping :", errping)
		errping = nil
		if errping = DbOpenMssql(); errping != nil {
			logrus.Error("Try to connect again but error :", errping)
		}
	}
	DbConMssql.LogMode(envMSSQLDb.Debug)
	return DbConMssql
}
