module proj-medanjaya-scheduler

go 1.15

require (
	github.com/denisenkom/go-mssqldb v0.0.0-20200206145737-bbfc9a55622e // indirect
	github.com/jasonlvhit/gocron v0.0.1
	github.com/jinzhu/gorm v1.9.16
	github.com/jinzhu/now v1.1.1 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/kr/text v0.2.0 // indirect
	github.com/lib/pq v1.3.0 // indirect
	github.com/mattn/go-sqlite3 v2.0.3+incompatible // indirect
	github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.5.1 // indirect
	github.com/t-tiger/gorm-bulk-insert v1.3.0
	github.com/t-tiger/gorm-bulk-insert/v2 v2.0.1
	golang.org/x/crypto v0.0.0-20200406173513-056763e48d71 // indirect
	gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
	gopkg.in/yaml.v2 v2.2.8 // indirect
)
